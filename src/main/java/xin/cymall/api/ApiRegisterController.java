package xin.cymall.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.cymall.common.api.annotation.IgnoreAuth;
import xin.cymall.common.redis.RedisCache;
import xin.cymall.common.redis.RedisEvict;
import xin.cymall.common.utils.R;
import xin.cymall.entity.User;
import xin.cymall.service.UserService;

/**
 * 注册
 * @author chenyi
 * @email 228112142@qq.com
 * @date 2017-03-26 17:27
 */
@RestController
@RequestMapping("/api")
public class ApiRegisterController {

    @Autowired
    private UserService userService;

    /**
     * 注册
     */
    @PostMapping("register")
    @IgnoreAuth
    //清除redis缓存
    @RedisEvict(type=User.class)
    public R register(String username,String mobile, String password){

        userService.save(username,mobile, password);

        return R.ok();
    }
}
