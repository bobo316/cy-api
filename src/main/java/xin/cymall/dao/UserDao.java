package xin.cymall.dao;


import xin.cymall.entity.User;

/**
 * 用户
 * 
 * @author chenyi
 * @email 228112142@qq.com
 * @date 2017-03-23 15:22:06
 */
public interface UserDao extends BaseDao<User> {

    User queryByUsername(String username);
}
